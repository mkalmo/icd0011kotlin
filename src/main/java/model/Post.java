package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Post {

    private Long id;

    @NotNull
    @Size(min = 2, max = 10)
    private String title;

    @NotNull
    @Size(min = 2, max = 12)
    private String text;

}
