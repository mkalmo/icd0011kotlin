import { deletePost,
         getAllPosts, addPost } from "./dao.js"

import { getMessage } from "./messages.js"

const { createApp } = Vue;

createApp({
    data() {
        return {
            newPost: {},
            posts: [],
            errors: []
        };
    },

    async created() {
        this.posts = await getAllPosts();
    },

    methods: {
        async deletePost(id) {
            await deletePost(id);
            this.posts = await getAllPosts();
        },

        async addPost(post) {
            try {
                await addPost(post);

                this.errors = [];
                this.newPost = {};

            } catch (error) {
                this.errors = error.messages.map(
                    e => getMessage(e.code, e.arguments));
            }

            this.posts = await getAllPosts();
        }
    }

}).mount('#container');