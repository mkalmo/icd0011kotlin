
export async function getAllPosts() {
    return (await fetch("api/posts")).json();
}

export async function deletePost(id) {
    return fetch(`api/posts/${id}`, {
        method: 'DELETE'
    });
}

export async function addPost(post) {
    const response = await fetch(`api/posts`, {
        method: 'POST',
        headers: { "Content-type": "application/json" },
        body: JSON.stringify(post)
    });

    if (response.status === 400) {
        const body = await response.json();
        throw {
            messages: body.errors ?? []
        }
    }

    return response;
}

